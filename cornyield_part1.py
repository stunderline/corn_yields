### Linear trend estimation for US corn yields using OLS, removing outliers, and Theil-Sen.
### Script for the analysis and plot generation of part 1 of the blog posts
### on the trend in US corn yields on https://codingandmore.home.blog/.
### The code is structured with VS Code cells delimited by #%% .

#%%
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.formula.api as smf
from scipy import stats

### Configure
matplotlib.rcParams['figure.figsize'] = (12.0, 6.0)
STATE = 'US TOTAL'

#%%
### Read the file and extract relevant columns for given state.
df = pd.read_csv('data/US_corn_yield.csv')
relevant_columns = ['State','Data Item', 'Year','Value']
df = df[relevant_columns]
df = df[df.State == STATE].reset_index()

#%%
### Plot the evolution of the corn yields since 1900
ax = df.set_index('Year')['Value'].plot(logy=False)
ax.set_ylabel('Yield\n[bu/ac]', rotation=0, ha='right', va='top')
ax.set_title('Evolution of US Corn Yield, 1900-2019')
plt.savefig('us_corn_yield_evolution.png')

# %%
### Filter by year - from now on everything starts in 1960.
LINEAR_START = 1960
df = df[df.Year >= LINEAR_START]

#%%
### Make a linear fit using numpy and plot the fit vs the measured data
fit = np.poly1d(np.polyfit(df.Year, df.Value, deg=1))
y_fit = fit(df.Year)
ax = df.set_index('Year')['Value'].plot(label='measured', marker='.')
ax.plot(df.Year, y_fit, label='linear fit', ls='--')
ax.set_ylabel('Yield\n[bu/ac]', rotation=0, ha='right')
ax.set_title('Evolution of US Corn Yield, 1960-2019')
plt.legend()
plt.savefig('us_corn_yield_linear_fit.png')

# %%
### Generate the plot for the residual distribution of the linear fit.
residuals = df.Value - y_fit
plt.figure()
plt.scatter(df.Year, residuals)
plt.figure()
sns.distplot(residuals/df.Value, rug=True)
plt.title('Distribution of Residuals')
plt.xlabel('Relative residual (%-deviation of observed)')
plt.ylabel('probability density')
plt.savefig('linear_fit_residuals_relative.png')
plt.figure()
sns.distplot(residuals, rug=True)
plt.title('Distribution of Residuals')
plt.xlabel('Residual [bu/ac]')
plt.ylabel('probability density')
plt.savefig('linear_fit_residuals_absolute.png')

#%%
### Run robust regression and generate plots with results

### Use the Theil-Sen estimator for a robust estimation
med_slope, med_intercept, _, _ = stats.theilslopes(df.Value, df.Year)

### Remove outliers where residuals are > CAP % of the value and run OLS on inliers
CAP = 0.2
relative_residuals = residuals / df.Value
inlier = relative_residuals.abs() < CAP
df_inlier = df[inlier]
fit_inlier = np.poly1d(np.polyfit(df_inlier.Year, df_inlier.Value, deg=1))
y_fit_inlier = fit_inlier(df_inlier.Year)

### Generate plots
plt.figure()
plt.plot(df.Year, df.Value, label='measured', marker='.')
plt.plot(df.Year, y_fit, label='OLS', ls='--')
plt.plot(df.Year, med_intercept + med_slope*df.Year, label='theil-sen', ls='--')
plt.plot(df_inlier.Year, y_fit_inlier, label='OLS - removed outlier', ls='--')
plt.scatter(df.Year[~inlier], df.Value[~inlier], color='black', marker='x', label='"outlier"')
plt.xlabel('Year')
plt.ylabel('Yield\n[bu/ac]', rotation=0, ha='right')
plt.title('Evolution of US Corn Yield, 1960-2019')
plt.legend()
plt.savefig('us_corn_yield_robust_liner_fit.png')

#%%
### Backtest all three methods (OLS, Theil-Sen, OLS with removed outliers) and compute RMSE/MAE

### Uniform interface for the three methods used in backtesting
def _ols(df: pd.DataFrame, prediction_year: float) -> float:
    """Standard OLS via polyfit"""
    fit = np.poly1d(np.polyfit(df.Year, df.Value, deg=1))
    y_fit = fit(prediction_year)
    return y_fit

def _ols_remove_outlier(df: pd.DataFrame, prediction_year: float, cap: float=0.2, return_outliers:bool =False) -> float:
    """Iteratively remove outliers defined by the relative size of the residuals.
    Iteration is required because removing an outlier and a subsequent regression can lead to new outliers.
    """
    outliers_exist = True
    to_remove = None
    outliers = set()
    MIN_LEN_OF_PREDICTOR = 5
    while outliers_exist and len(df) >= MIN_LEN_OF_PREDICTOR:
        if to_remove is not None:
            print(f'{prediction_year} - removing values: {df[to_remove].Year.values}')
            outliers.update(df[to_remove].Year.values)
            df = df[~to_remove]
        ols_fit = np.poly1d(np.polyfit(df.Year, df.Value, deg=1)) 
        y_fit = ols_fit(df.Year)
        residual_rel = (df.Value - y_fit)/df.Value
        to_remove = residual_rel.abs() > cap
        outliers_exist = True if sum(to_remove) > 0 else False
    if return_outliers:
        return outliers
    else:
        return ols_fit(prediction_year)

def _theil_sen(df: pd.DataFrame, prediction_year: float) -> float:
    """Linear fit using Theil-Sen estimator"""
    med_slope, med_intercept, _, _ = stats.theilslopes(df.Value, df.Year)
    return med_intercept + med_slope * prediction_year

MODEL_FIT = {
    'OLS' : _ols,
    'theil-sen': _theil_sen,
    'OLS - removed outliers' : _ols_remove_outlier,

}

### Backtesting functions using above methods

def backtest_model(df: pd.DataFrame, start_year: int=1981, end_year: int=2019) -> pd.DataFrame:
    """Fits Value ~ Year for all years for all models"""
    result = []
    for year in range(start_year, end_year+1):
        df_year = df[df.Year < year]
        for model_name, fn in MODEL_FIT.items():
            predicted = fn(df_year, year)
            residual = df[df.Year == year]['Value'].values[0] - predicted
            result.append({'year': year, 'model_name': model_name, 'residual': residual, 'predicted': predicted})
    return pd.DataFrame(result)

def compute_metrics(df_res: pd.DataFrame) -> pd.DataFrame:
    """Compute MAE and RMSE from the result of backtest_model()"""
    metrics = []
    for year in df_res.year:
        for model_name in MODEL_FIT.keys():
            df = df_res[(df_res.model_name==model_name) & (df_res.year < year)]
            mae = df.residual.abs().mean()
            rmse = np.sqrt((df.residual*df.residual).mean())
            metrics.append({'model_name' : model_name, 'metric': 'MAE', 'year' : year, 'value' : mae})
            metrics.append({'model_name' : model_name, 'metric': 'RMSE', 'year' : year, 'value' : rmse})
    return pd.DataFrame(metrics)

def get_outlier_years(df: pd.DataFrame, start_year: int=1981, end_year: int=2019) -> pd.DataFrame:
    """Create a dataframe containing the list of outliers by each year"""
    outliers = []
    for year in range(start_year, end_year+1):
        outliers.append({'year' : year, 'outlier' :_ols_remove_outlier(df[df.Year < year], year, return_outliers=True)})
    return pd.DataFrame(outliers)

def compute_metrics_ex_outlier(df_res: pd.DataFrame, outlier: pd.DataFrame) -> pd.DataFrame:
    """Compute MAE and RMSE from the result of backtest_model when ignoring outliers years
    as classified by the ols_remove_outlier function"""
    metrics = []
    for year in df_res.year:
        outlier_year = outlier[outlier.year == year]['outlier'].values[0]
        for model_name in MODEL_FIT.keys():
            df = df_res[(df_res.model_name==model_name) & (df_res.year < year)]
            df = df[~df.year.isin(outlier_year)]
            mae = df.residual.abs().mean()
            rmse = np.sqrt((df.residual*df.residual).mean())
            metrics.append({'model_name' : model_name, 'metric': 'MAE', 'year' : year, 'value' : mae})
            metrics.append({'model_name' : model_name, 'metric': 'RMSE', 'year' : year, 'value' : rmse})
    return pd.DataFrame(metrics)

backtest = backtest_model(df)
outliers = get_outlier_years(df)
metrics = compute_metrics(backtest)
metrics_ex_outlier = compute_metrics_ex_outlier(backtest, outliers)

#%%
### Generate plots for the RMSE/MAE evolution over time for the different estimators
palette = sns.color_palette()[1:4]
g = sns.lineplot(data=metrics, x='year', y='value', hue='model_name', style='metric', palette=palette[:3])
g.set_xlabel('Year')
g.set_ylabel('Value')
g.set_title('Error metrics US corn yield model: Backward looking expanding window')
plt.savefig('error_metrics.png')

plt.figure()
g = sns.lineplot(data=metrics_ex_outlier, x='year', y='value', hue='model_name', style='metric', palette=palette)
g.set_xlabel('Year')
g.set_ylabel('Value')
g.set_title('Error metrics US corn yield model: Backward looking expanding window\nexcluding outlier years')
plt.savefig('error_metrics_ex_outlier.png')

# %%
### Predictions for 2020
print(f'OLS 2020: {_ols(df, 2020)}')
print(f'Theil-Sen 2020: {_theil_sen(df, 2020)}')
print(f'OLS (removed outlier) 2020: {_ols_remove_outlier(df, 2020)}')
